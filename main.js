$(document).ready(() => {
    const $hamburgerButton = $('.hamburger_button');
    const $dropDown = $('.dropdown');
    const $closeButton = $('.close_button')

    $hamburgerButton.on('click', () => {
        $dropDown.show();
        $closeButton.show();
        $hamburgerButton.hide();
    });
    $closeButton.on('click', () => {
        $dropDown.hide();
        $hamburgerButton.show();
        $closeButton.hide();
    })

});